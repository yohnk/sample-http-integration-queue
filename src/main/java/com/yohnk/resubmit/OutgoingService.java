package com.yohnk.resubmit;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class OutgoingService {

    @Autowired
    public MessageGateway gateway;

    //http://www.enterpriseintegrationpatterns.com/patterns/messaging/MessagingAdapter.html
    //A service activator is another abstraction that hides how the message is actually getting delivered
    //Change the inputChannel to "inMemoryChannel" to keep the messages internal
    //Change the inputChannel to "inboundJMSChannel" to handleMessage to our Message Queue
    @ServiceActivator(inputChannel="inMemoryChannel")
    public void handleMessage(String s){
        try{
            HttpPost httpPost = new HttpPost("http://test.local");
            httpPost.setEntity(new StringEntity(s));
            CloseableHttpResponse response2 = HttpClients.createDefault().execute(httpPost);
        }
        catch (IOException e) {
            //If the request fails, resubmit it to the queue
            //More than likely you'd want to look into a delay queue to slow down resubmissions
            //You could also look into tracking the num failures and send to a dead letter queue at some point
            e.printStackTrace();
            gateway.send(s);
        }
    }

}
