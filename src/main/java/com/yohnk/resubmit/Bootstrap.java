package com.yohnk.resubmit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

//Spring Boot is a framework that deals with a lot of the boilerplate configuration for us.
//A "@SpringBootApplication" is the bootstrapping class.

@SpringBootApplication

//We need to explicitly tell spring to read in our JMS configuration file
@ImportResource("spring-jms-beans.xml")
public class Bootstrap {
    public static void main(String[] args) {
        SpringApplication.run(Bootstrap.class, args);
    }
}
