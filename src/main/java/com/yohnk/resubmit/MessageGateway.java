package com.yohnk.resubmit;

import org.springframework.integration.annotation.MessagingGateway;

//http://www.enterpriseintegrationpatterns.com/patterns/messaging/MessagingGateway.html
//A gateway is an interface that we can hide the actual message transport mechanism behind
//Change the defaultRequestChannel to "inMemoryChannel" to keep the messages internal
//Change the defaultRequestChannel to "outboundJMSChannel" to handleMessage to our Message Queue
@MessagingGateway(defaultRequestChannel = "inMemoryChannel")
public interface MessageGateway {

    public void send(String message);

}
