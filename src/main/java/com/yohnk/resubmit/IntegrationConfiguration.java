package com.yohnk.resubmit;

import org.apache.activemq.artemis.jms.client.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.jms.Jms;
import org.springframework.messaging.MessageChannel;

import javax.jms.ConnectionFactory;


@Configuration
@ComponentScan("com.yohnk.resubmit")
@IntegrationComponentScan("com.yohnk.resubmit")
@EnableIntegration
public class IntegrationConfiguration {

    @Autowired
    ConnectionFactory connectionFactory;

    @Autowired
    ActiveMQQueue externalQueue;

    @Bean
    //This getter maps to the annotation in MessageGateway.java
    public MessageChannel inMemoryChannel() {
        return new DirectChannel();
    }

    @Bean
    public MessageChannel outboundJMSChannel() {
        return new DirectChannel();
    }

    @Bean
    public MessageChannel inboundJMSChannel() {
        return new DirectChannel();
    }


    @Bean
    public IntegrationFlow outboundFlow() {
        return IntegrationFlows.from("outboundJMSChannel").handle(Jms.outboundAdapter(connectionFactory).destination(externalQueue.getQueueName())).get();
    }

    @Bean
    public IntegrationFlow inboundFlow() {
        return IntegrationFlows.from(Jms.inboundGateway(connectionFactory).destination(externalQueue.getQueueName())).channel("inboundJMSChannel").get();
    }


}