package com.yohnk.resubmit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//Spring MVC (part of boot) allows us to create pretty minimal HTTP controllers based on some annotations
@RestController
public class IncomingController {

    @Autowired
    //Since we added the "@MessagingGateway" to the MessageGateway interface, spring will automatically create us one and connect it here
    //The channel it submits to is defined in the MessageGateway interface
    public MessageGateway gateway;

    @RequestMapping("/")
    //All requests made to http://host:8080 will be mapped here
    //The body of the HTTP request will be passed in as a String
    //Spring MVC also has a very robust parsing library, so this parameter could also be a POJO representing the request
    public ResponseEntity<String> simple(@RequestBody String body) {
        //Simply sent the string over our gateway and return a 200
        //This could be configured to pass POJOs as well
        gateway.send(body);
        return new ResponseEntity<String>(HttpStatus.OK);
    }

}
