# HTTP Resubmit Queue

An example project showcasing how to receive, transform and re-submit an HTTP request. Specific emphasis is put on persisting the message in case of failure and retrying the request in the situation that the final endpoint is down.

#### Overview

* [IntegrationConfiguration.java]() - The file we use to define how messages flow through the system.
* [IncomingController.java]() - Our inbound HTTP controller.
* [OutgoingService.java]() - Used to re-submit messages to another HTTP service.
* [broker.xml]() - Configuration for our Message Queue (Artemis).
* [spring-jms-beans.xml]() - Configuration that defines our Message Queue to Spring and makes it usable by our application.

### Message Flow

Internal Flow -
````
IncomingController --> MessageGateway --> inMemoryChannel --> OutgoingService
````

Message Queue Flow -
`````
IncomingController --> MessageGateway --> outboundJMSChannel --> externalQueue (Artemis) --> inboundJMSChannel --> OutgoingService
`````

### Persistance

The Internal Flow does not persist messages. They are only stored in memory and if the application fails you lose your messages.

The Message Queue Flow allows for persistance via the broker.xml file. See the Artemis Configuration link below for details.

### Retry

When a message fails to resubmit in our OutgoingService, we resubmit it to the orginal MessageGateway to try again. This would be best to couple with a Delay Queue and possibly track failures so we're not retrying things often and indefinitely. 

#### Helpful Links

* [Artemis Configuration](https://activemq.apache.org/artemis/docs/1.0.0/configuration-index.html) - Configuration options for the broker.xml file.
* [Spring Integration DSL](https://github.com/spring-projects/spring-integration-java-dsl/wiki/spring-integration-java-dsl-reference) - A reference guide for Spring Integration's DSL. This is what we're using to define our IntegrationFlow objects.
* [Artemis & Spring Integration](https://github.com/apache/activemq-artemis/tree/master/examples/features/standard/spring-integration/src/main/java/org/apache/activemq/artemis/jms/example) - An example project hosted by Apache to showcase how Spring Integration & Artemis work together.
